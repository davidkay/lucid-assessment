# Lucid Tech Assessment

This repo includes the 'single-file' Ansible playbook for David Kay. `lucid-assessment.yml` includes three plays:

1. bootstrap the vm with python
2. install and config apache
3. install and config the python monitoring script

## Quickstart

I've included a `Vagrantfile` to provision a vagrant box in virtualbox...in the root directory just run `vagrant up`.

`lucid-assessment.yml` can also be run from the CLI with standard options such as username, password, inventory, verbosity, etc.

`ansible-playbook -u <USERNAME> -i 192.168.50.50, lucid-assessment.yml -vvv`

## Assessment requirements

1. Deploy an apache web server
  1. It must serve and html file with the text "Hey Lucid! This is David Kay assessment".
  1. The server must be deployed and configured via Ansible.
  1. Provisioning from a single ansible-playbook command.
2. Deploy a python script to monitor the Apache server
  2. The script must deployed and configured via Ansible
  2. The monitor must run in 1 minute intervals
  2. If the server goes down, the script must write to a file called "alert.txt" the phrase "YYYY-MM-DD HH:mm SERVER is DOWN"
  2. If the server is up but doesn't return "Hey Lucid! This is David Kay assessment" the script must write to a file called "alert.txt" the phrase "YYYY-MM-DD HH:mm SERVER is RETURNING WRONG MESSAGE"
  2. If the server is not returning 200 HTTP codes, the script must write to a file called "alert.txt" the phrase "YYYY-MM-DD HH:mm SERVER is NOT OK" 3 - The assessment will executed on a Ubuntu 16.04 virtualbox with root access

## Bootstrap

Many systems don't have python installed by default. This bootstraps the system with python2 and pip with sudo.

```yaml
- name: Bootstrap system with python & pip
  hosts: all
  tasks:
    - name: Install python & pip
      raw: apt update && apt install -y python python-pip
      become: true
```

## Install Apache

Installs apache and starts the apache service.

```yaml
...
  tasks:
    - name: Installing apache...
      apt:
        name: "{{ packages }}"
        update_cache: yes
      vars:
        packages:
          - apache2
      become: yes
    - name: Starting Apache...
      service: name=apache2 state=started
      become: yes
```

## Deploy index file

Removes the default apache index.html and deploys a new file using lineinfile module with appropriate user, groups, perms & a regex check to delete everything just in case. `alert.txt` link will 404 if no exceptions have been written yet.

```yaml
...
    - name: Deleting default index.html...
      file:
        path: /var/www/html/index.html
        state: absent
      become: yes
    - name: write index.html to vm
      lineinfile:
        path: /var/www/html/index.html
        owner: www-data
        group: www-data
        mode: 0644
        create: yes
        regexp: '(.*\n*)*'
        line: '<!DOCTYPE html><html><head><meta charset="utf-8"><meta name="viewport" content="width=device-width, initial-scale=1"><title>Lucid Assessment</title><link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.1/css/bulma.min.css"><script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script></head><body><section class="hero is-fullheight is-dark bold"><div class="hero-body"><div class="container"><h1 class="title">Hey Lucid!</h1><h1 class="subtitle is-4">This is <strong class="has-text-primary">David Kay</strong> assessment.</h1><a class="button is-danger" href="alert.txt"><span class="icon is-small"><i class="fas fa-exclamation-triangle"></i></span><span>alert.txt</span></a></div></div></section></body></html>'
      become: yes
```

## Deploy a python script

`lucid-python-monitor` is a simple script that checks for running processes, status codes and parses site text and writes to `alert.txt` in the `/var/www/html/` directory so it can be accessed through link on index.html (unless apache is down, obviously. `cat /var/www/html/alert.txt` will get it then.)

* uses subprocess to get a list of running proccesses
  * if apache2 is not in running processes, write to `alert.txt`
* if apache2 is in running processes, use `requests` to pull status code and data
  * if status_code is not 200, write to `alert.txt`
* if status code is 200, parse index.html for text
  * use beautifulsoup html.parser and parse index.html to `soup`
  * check `soup.text` against required string, and if it doesn't match, write to `alert.txt`

```yaml
...
  tasks:
    - name: Installing beautifulsoup...
      pip:
        name: beautifulsoup4
        executable: pip
      become: yes
    - name: Deploying lucid-python-monitor.py...
      blockinfile:
        path: ~/lucid-python-monitor.py
        create: yes
        block: |
          #!/usr/bin/python
          import subprocess, requests
          from datetime import datetime
          from bs4 import BeautifulSoup
          
          output = subprocess.getoutput("ps -A")
          if not 'apache2' in output:
            alert = open('/var/www/html/alert.txt', 'a')
            alert.write(datetime.now().strftime('%Y-%m-%d %H:%M') + ' SERVER is DOWN' + '\n')
            alert.close()
          elif 'apache2' in output:
            url = requests.get('http://localhost/index.html')
            if url.status_code != 200:
              alert = open('/var/www/html/alert.txt', 'a')
              alert.write(datetime.now().strftime('%Y-%m-%d %H:%M') + ' SERVER is NOT OK' + '\n')
              alert.close()
            else:  
              soup = BeautifulSoup(url.text, 'html.parser')
              text = 'Hey Lucid!This is David Kay assessment.'
              if text not in soup.get_text():
                alert = open('/var/www/html/alert.txt', 'a')
                alert.write(datetime.now().strftime('%Y-%m-%d %H:%M') + ' SERVER is RETURNING WRONG MESSAGE' + '\n')
                alert.close()
```

## Run the script every minute

Just use cron.

```yaml
    - name: Creating recurring cron job for script...
      cron:
        name: "Python Monitor"
        minute: "*"
        job: "sudo python lucid-python-monitor.py"
```

> Easier to read version of `lucid-python-monitor.py`

```python
#!/usr/bin/python
import subprocess, requests
from datetime import datetime
from bs4 import BeautifulSoup

output = subprocess.getoutput("ps -A")
if not 'apache2' in output:
  alert = open('/var/www/html/alert.txt', 'a')
  alert.write(datetime.now().strftime('%Y-%m-%d %H:%M') + ' SERVER is DOWN' + '\n')
  alert.close()
elif 'apache2' in output:
  url = requests.get('http://localhost/index.html')
  if url.status_code != 200:
    alert = open('/var/www/html/alert.txt', 'a')
    alert.write(datetime.now().strftime('%Y-%m-%d %H:%M') + ' SERVER is NOT OK' + '\n')
    alert.close()
  else:  
    soup = BeautifulSoup(url.text, 'html.parser')
    text = 'Hey Lucid!This is David Kay assessment.'
    if text not in soup.get_text():
      alert = open('/var/www/html/alert.txt', 'a')
      alert.write(datetime.now().strftime('%Y-%m-%d %H:%M') + ' SERVER is RETURNING WRONG MESSAGE' + '\n')
      alert.close()
```