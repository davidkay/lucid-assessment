# -*- mode: ruby -*-
# vi: set ft=ruby :

# Vagrantfile that automatically provisions with
# ansible-pb-lucid-assessment.yml file
Vagrant.configure("2") do |config|
  
  # define a base box and check if there's an updated version
  config.vm.box = "ubuntu/xenial64"
  config.vm.box_check_update = true
  config.ssh.insert_key = false

  # assign a hostname and make the vm available at a 
  # private IP address
  config.vm.hostname = "lucid-assessment-web"
  config.vm.network "private_network", ip: "192.168.50.50"

  # give the vm a different name because 'default' is boring
  config.vm.define "lucid-assessment" do |t| end

  # use virtualbox as the provider and set up
  # some specs for the VM
  config.vm.provider "virtualbox" do |v|
    v.name = "lucid-web"
    v.memory = 1024
    v.cpus = 1
    v.linked_clone = true
    v.gui = false
  end

  # run built-in ansible provisioning...
  # you can comment this out to run manually
  config.vm.provision "ansible" do |ansible|
    ansible.verbose = true
    
    ansible.extra_vars = {
      ansible_python_interpreter: "/usr/bin/python3",
    }
    ansible.playbook = "lucid-assessment.yml"
  end
end
